### IAM Advanced


### Aws Directory Service

#### Servicos

- Managed Microsoft AD
- AD Connector
- Simple AD
- Cloud Directory ( NOT AD Compatible )
- Cognito user pools ( NOT AD Compatible )


### IAM Policies

Not explicily allowed == implicity denied

Explicit deny > everything else

Somente politicas atachadas tem efeito


#### ARN

Como é um ARN

***arn:partition:service:region:account_id:***


Ai finaliza com

resource
resource_type/resource
resource_type/resource/qualifier
resource_type/resource:qualifier
resource_type:resource
resource_type:resource:qualifier


Exemplo

***arn:aws:dynamodb:us-east-1:321321321321:table/orders***

***arn:aws:iam::132132131321:user/kelvyn***

Esse acima tem um ***::*** pq é um servico global

#### Policy Documents

é um JSON

Um policy Document é uma lista de statements


Cada statement é uma request a uma api da aws

´´´
{
  "Version": "2012-10-17"
  "Statement":[

  ]
}
´´´


### Permission Boundaries ( Estudar melhor )

- Delegate Administration to other users



---

### Resource Access Manager (RAM)

é o metodo de compartilhar recursos entre contas distintas da aws.

Exemplo: A conta 22 tem um banco de dados Dynamodb, e a conta 33 quer ter acesso a esse recurso.

Vocẽ habilita o compartilhamento com o RAM e a outra conta precisa aprovar o compartilhamento no mesmo painel do RAM e assim que aprovado, as duas contas compartilham o mesmo recurso de banco.

isto funciona somente para alguns recursos

---

### AWS SSO

É um portal de acesso ao SSO para várias aplicações que tenha SAML 2.0

Se tiver uma pergunta com SAML 2.0, procure nas respostas o SSO
