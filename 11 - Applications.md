# Application

# SQS

SQS é um servico de fila que vocẽ pode inserir dados para outra parte da aplicação processar esses dados

na prova podera have algo como decoupling ou desacoplar, relacione isso com o SQS

2 tipos de filas:

- Standard
  Metodo com maior thorughtput, mas as mensagens podem chegar fora de ordem e multiplas vezes.

- Fifo  
   First-in-firts-out, metodo que segue uma sequencia, mas tem um menor throughput.

SQS is pull-based

Mensagens podes ser guardadas de 1 min a 14 dias, default 4 dias.


Visibility Timeout é o tempo que a mensagme fique invisivel enquanto está sendo processado, se for processado com sucesso, a mensagem é deletada, se não a mensagem expira o timeout e ela fica visivel novamente.


# SWF

Disparo de rotinas, na prova sehouver algo envolvendo interação humana, como pegar um produto numa prateleira e tenha pesquisa disso e'com o SWF


# SNS

é um servico de mensagens, consegue enviar para dispositivos móveis com push notification, email e sms

Ele trabalha como Push, enquanto o SQS é Pull

# Elastic Transcoder

é um transcoder para varios dispositivos na internet.

# API Gateway

é um servico de "frontend de api", que consegue criar regras e determinar cada endpoint e o seu destino.

Serverless


# Kinesis

é um servico de streaming de dados ( e não de video ), é usado para dados gerados continuamento, como hashtag do twitter, localização de ubers no app etc.

3 tipos

- Kinesis streams
  é um straming de dados que envia para consumindores armazenado dentro de shards
  dados armazenados de 24h até 7 dias

- Kinesis Firehose
  é um analizador de dados que não persiste, somente dispara o dado para ser processado.

- Kinesis Analytcs
  O kinesis analytcs ele faz o processamento e/ou a analise dentro do proprio kinesis, ao contrario do firehose que dispara para processamento.

# Cognito

Provem Autenticação federada, Web identity federation para apps

pode usar facebook google ou amazon para autenticar

- User Pool
  é a parte de autenticação de usuário, cadastro e recuperação

- Identity pool
  é a parte que autoriza acessos nos seus recursos da aws
