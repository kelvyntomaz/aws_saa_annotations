# Route53

ELBs Não tem pre definido um IPv4 address, sempre é resolvido com uma entrada DNS

A diferenca entre um Cname e um Alias

Se tiver uma questao com cname e alias, sempre escolha o alias

### Routing Policies

- Simple Routing
  - 1 Record, Multiple ips, Random ip

- Weigted Routing
  - é um Roteamento por peso, pode colocar peso em varias respostas.

- Latency-based Routing
  - Ele se baseia por latencia para as regiões e manda para a região mais próxima do usuário.

- Failover Routing
  - Ele entrega baseado em ativo e passivo, com health check, se o ativo cair, ele muda para o passivo.

- Geolocation Routing
  - Ele encaminha os usuários para a região determinada pela geolocalização setada pelo sysadmim.

- Geoproximity Routing ( Traffic Flow Only)

  - é mais utilizado para basear o trafego por lat e lon, somente dá pra usar com traffic flow.

- Multivalue Answer Routing
  - Parecido com o Simple Routing, mas ele só responde recursos saudáveis, pois habilita health check.
