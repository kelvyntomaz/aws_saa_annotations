# VPC

## Tips: Se vocẽ antes da prova conseguir montar uma VPC de cabeca, voce está apto a fazer a prova.

A maior subnet que você pode ter na AWS é um /16 e a menor subnet que pode ter é um /28

VPC é um "datacenter logico " na AWS

1 Subnet = 1 AZ

Security Groups é Stateful, Network Access Control Lists é Stateless

Não Tem transitive Peering, ara Fazer a VPC Peering tem que ser com cada VPC que quer se comunicar, não tem como fazer o "roteamento" para passar dentro de uma vpc para acessar a outra.

Quando vc cria uma VPC, via ser criado automaticamente uma defautl Route Table, Network ACL e um Default Security Group

Se não criar nenhuma Subnet, não será possivel criar um IGW

As AZs são Randomizadas, sua 1a pode não ser a mesma 1a de outra conta

Vc só tem 1 IGW por VPC

### IPs Reservados numa Subnet

10.0.0.0: network
10.0.0.1: AWS VPC Router
10.0.0.2: DNS Server
10.0.0.3: Reserved for future use
10.0.0.255: Broadcast

### Sequencia de Conexão

Internet Gateway(IGW) -> Router -> Router Table - > Network ACL -> Security Group


### NAT Na VPC

NAT Gateway precisa estar numa subnet Publica

2 Maneiras de ter nat no Security Group para acesso a internet por exemplo

- Nat Instance
  é uma instancia em ec2 que irá fazer a rota ( menos Recomendado e Outdated, não usar)

- Nat Gateway
  é uma maneira de fazer o nat com gateway com HA

### ACL

Sempre vem com uma ACL Default Liberando tudo

Vocẽ pode criar ACL Custom

Toda Subnet precisa ser associada com uma ACL

Você Bloqueia IPs Na ACL e não na Subnet


1 Subnet - 1 ACL

1 ACL - N Subnets

ACl é Stateless

### VPC Flow Logs

Log de Trafego, pode ser feito no nive de VPC e Elastic network Interface

Criado o Flow Log, não dá para alterar o mesmo.

Não é todo o trafego é monitorado, DNS e trafego para as ferramentas da AWS não são logados, DHCP tbem.

### bastion host

Jump Host, é só um host para acesso a infra


### Direct Connect

Conecta o seu datacenter na AWS

Utilizado para high Throughtput ou stable connection.

Assistir o video da AWS ensinando a fazer o direct connection com VPN !!!!!!!!!!!


### AWS Global accelerator

ESTUDAR PQ NUM ENTENDI NADAAAA!!!!


### VPC Endpoints

é um Endpoint que pode ser criado para acessar servicos na aws sem passar pela internet, indo diretamente pela rede da AWS

Tem Interface e Gateway

### VPC privateLink

é uma maneira de interligar VPCs Sem o VPC peering e nm publicar o servico na internet.

é melhor que o VPC peering em casos que são muitas VPC, na prova poderá ter uma pergunta falando sobre 10, 100 ou 1000 VPC e ai que entra o privateLink

Precisa ter um NLB na VPC de servico e uma ENI na VPC customer


### AWS Transit Gateway

Seria Basicamente um Hub que Interliga varias VPCs e Vpns e Direct connects

Pode Ter em Multiplas regiões

Pode usar route tables Para limitar as redes

***Suporta IP Multicast***

### VPN Cloud Hub

é basicamente um Hub de VPN que conecta Multiplas localidades de VPN com a VPC e Voce consegue trafegar entre outras localidades através do CLoud HUB, parecido um pouco com o Transit Gateway, mas é focado em VPN.


### Network Costs

prefira usar a comunicação pelo IP Privado invés de Ip Publico pois diminui os custos.

Se precisar ser custo zero, coloque tdos os EC2 na mesma AZ, mas isso tirará o poder de HA.
