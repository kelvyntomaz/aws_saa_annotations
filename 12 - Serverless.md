# Serverless

## Lambda

Lambda é um servico serverless que executa tarefas de computing

é cobrado pelo tempo de execução e pela chamada, os primeiros 1 milhão não sao cobrados

Scale out automatically

AWS x-ray para debug

Lambda pode fazer coisas globalmente.

# SAM

Serverless Application Model

Extensao de Cloudformation

Cria o enviroment de Serverless, como por exemplo criar o lambda já com a api aws_api_gateway_gateway_response

Tem o SAM cli para criar os templates para rodar no cloudformation.

# ECS

Elastic Container Service

Administra orquestração de container

Cluster para os containers, com ec2 ou não utilizando o servico do fargate.

# Fargate

Funciona com ECS e com EKS

Serverless Container Engine

# EKS

Elastic Kubernetes Service

# ECR

Elastic Container Registry

é o "DockerHub" do seu ambiente
