### Databases


- Multi-AZ - For Disaster Recovery
- Read Replicas - For Performance

#### OLTP vs OLAP

- OLTP - Online Transaction Processing, é um tipo query de uso para transações online, como por exemplo voce fazer um select numa compra de um usuário, irá trazer os dados que estão no banco.

- OLAP - Online Analytcs Processing, é um tipo de query para uso de analytcs Puxando muitos dados ao mesmo tempo, como por exemplo puxar dados de quantos radios foram vendidos na ameica latina, e qual é o custo de produção do radio naquele momento.

#### RDS (OLTP)

***RDS Não é Serverless, exceto o Aurora***

- SQL
- MySql
- PostgreSql
- Oracle
- Aurora
- MariaDB

#### DynamoDb (NoSQL)

O DynamoDB é um banco Não Relacional (noSQL) de chave e valor, rapido e flexivel  

O DynamoDB escala automaticamente a capacidade de throughput para atender às demandas das cargas de trabalho. Além disso, altera o particionamento dos dados à medida que o tamanho da tabela aumenta. Além disso, o DynamoDB replica dados de forma síncrona entre três instalações em uma região da AWS, proporcionado alta disponibilidade e resiliência de dados.

O DynamoDB não é Serverless, mas a capacidade e o gerenciamento do compute é totalmente da aws.

#### RedShift(OLAP)

Data Warehousing

#### ElasticCache

uso para melhor performance de databases já existentes

- Redis
- Memcached



### Backups

Os backups Não são restaurados no mesmo endpoint, são restaurados em outro endpoint

#### Automated Backups

- Feito em janelas pré agendadas

#### Db Snapshots

- Feitos manualmente

#### Read Replicas

- Precisa ter o backup habilitado.

- Pode ser multi az

- Pode ser em diferentes regioes

- Pode ser Mysql, PostgreSql, mariaDB, Oracle, Aurora

- Pode ser promovido para master, mas quebrará a replica


#### MultiAZ

- Usado para DR

#### Encryption

é Suportados pelos SQLs e até o aurora, é gerenciado pelo KMS


---


### DynamoDb

- NoSQL

- SSD Stoorage

- 3 Areas Spread

#### Eventual and Strong Consistent Reads

- Eventual Consistent Reads: Quando a aplicação grava o dado, e não precisa ler imediatamente.

- Strongly Consistent Reads: Quando a aplicação grava o dado e precisa retornar o resultado o mais rapido possivel.


#### DynamoDB accelerator (DAX)

- Cache gernciado pela AWS
- 10% Performance Improvement
- Reduz request de milisegundos para microsegundos
- Não precisa de desenvolverdor para gerenciar a logica de cache.


#### Point-in-Time Recovery (PiTR)

- Protege de Escrita ou deleçao sem querer
- Pode resturar em até 35 dias
- Não é habilitado por default

#### Global Tables

- Replication Multi-master, Multi-region
- Based on DynamoDB streams


---

### REDShift


Data Warehousing

Avaliable in 1 AZ

Pode ser Configurado como:

- Single Node ( 160G )
- Multi-node

#### Backups

- habilitado por defaul com 1 dia de retenção
- Maximo de Retenção é de 35 dias
- Redshift vai manter 3 copias no minimo dos dados, 2 em compute nodes e uma em s3
- Redshift pode replicar asyncrono para um outro s3 em outra região.

---

### Aurora

- Serverless

- High Performance and cost-effectively

- Mysql and Postgresql Compativeis

- 2 copies of or data in each az, minimum  3 az.

- Share Snapshots with other AWS Acconts

- Aurora tem backup automatico habilitado por padrao

- Use Aurura Serverless, simple cost-effective option for infrequent, intermitent or unpredictible workloads.


---

### ElastiCache

- Use to increase database and Web Application Performance.

- Redis is Multi AZ

- Can backup and restored redis

---

### Database Migration Service ( DMS )

- Pode Migrar On prem para cloud e vice e versa e tbem para on pprem -> On prem  e Cloud -> cloud

- Para migrar para basese diferentes, Ex: mysql para oracle, precisa usar o Schema conversion tool ( sct ). não utiliza se as bases forem as mesmas.


---
### EMR

- Big Date Processing

- master node, core node and a task node( opcional)

- default log data é armazenado no master node

- Pode Configurar a replicação dos logs para o s3 com 5 minutos de intervalo, e isso só pode ser configurado quando cria o cluster na primeira vez.
