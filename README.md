# Anotações AWS SAA

Este repositório contem todas as anotações feitas no curso da ACloudGuru para a Certificação Solution Architect


Temas em Sequencia

- [S3](01 - S3.md)
- [CloudFront](02 - Cloudfront.md)
- [Ec2](03 - EC2.md)
- [Cloudwatch](04 - Cloudwatch.md)
- [Databases](05 - Databases.md)
- [IAM](06 - IAM Advanced.md)
- [Route 53](07 - Route53.md)
- [VPC](08 - VPC.md)
- [ELB](09 - ELB.md)
- [AutoScalling](10 - AutoScalling.md)
- [Application](11 - Applications.md)
- [Security](12 - Security.md)
- [Serverless](13 - Serverless.md)

## Dicas!

- Leia o FAQ de recursos que você não domine tanto, lá tem tudo explicado e muitas respostas de perguntas da prova.


## Materiais Complementares


https://tutorialsdojo.com/aws-cheat-sheets/

https://tutorialsdojo.com/comparison-of-aws-services/

https://www.udemy.com/course/aws-certified-solutions-architect-associate-amazon-practice-exams-saa-c02/
